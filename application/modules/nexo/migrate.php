<?php
return array(
	'3.0.13'	=>	dirname( __FILE__ ) . '/migrate/3.0.13.php',
	'3.0.11'	=>	dirname( __FILE__ ) . '/migrate/3.0.11.php',
	'3.0.9'		=>	dirname( __FILE__ ) . '/migrate/3.0.9.php',
	'3.0.1'		=>	dirname( __FILE__ ) . '/migrate/3.0.1.php',
	'2.9.6'		=>	dirname( __FILE__ ) . '/migrate/2.9.6.php',
	'2.9.1'		=>	dirname( __FILE__ ) . '/migrate/2.9.1.php',
	'2.9.0'		=>	dirname( __FILE__ ) . '/migrate/2.9.0.php',
	'2.8.2'		=>	dirname( __FILE__ ) . '/migrate/2.8.2.php',
	'2.8.1'		=>	dirname( __FILE__ ) . '/migrate/2.8.1.php',
	'2.8.0'		=>	dirname( __FILE__ ) . '/migrate/2.8.0.php',
);
